# FS-1030-Wade-Andrew
# Admin page login:
# User: wade
# Password: password

## All SQL statements exist in the "controller.js" file in the "controllers" folder in the Back-End repo

## CRUD operations implemented on this site:
## CREATE message when visitor sends message from the contact page. Message is added to database.
## READ and display messages on the Admin page.
## READ data from the database to render portfolio and résumé pages.
## UPDATE messages by adding notes to messages. Notes are added to the database.
## DELETE messages from the Admin page, which also deletes them from the database.
